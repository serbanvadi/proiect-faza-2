In linia de comanda:
nvm install --lts or nvm install 10.13.0;
Varificam daca totul este ok: node -v and npm -v;
Configurare baza de date SQL
mysql-ctl install;
mysql-ctl start;
Retinem Root User si numele bazei de date;
Cream un proiect NodeJS:
npm init;
Express: npm install express --save;
Body-Parser npm install body-parser --save;
Sequelize npm install sequelize --save;
MySQL Connector: npm install mysql2 --save;
Automatic Restart on save of NodeJS Server
Node Monitor: npm install nodemon --save-dev;
Start NodeJS server:
./node_modules/nodemon/bin/nodemon.js index.js;

In POSTMAN: 
 Spre exemplu, pentru tabela Utilizator, pot alege metoda POST cu link-ul   https://serban-serbanadi.c9users.io/inregistrare si in Body, selectez raw si JSON si introduc:
{
	"nume":"Talaba",
	"prenume":"Adina",
	"tara":"Ro",
	"utilizator":"tadina",
	"parola":"12345"
}
Pentru metoda PUT, se utilizeaza, spre exemplu, link-ul https://serban-serbanadi.c9users.io/inregistrare/tadina
Acesta va modifica utilizatorul tadina cu orice informatii voi introduce in JSON, e.g.:
{
	"nume":"Talaba",
	"prenume":"Adina",
	"tara":"De"
}


Pentru metoda GET se utilizeaza link-ul: https://serban-serbanadi.c9users.io/get-all-users
Aceasta va intoarce toti utilizatorii.

Pentru metoda DELETE, link-ul: https://serban-serbanadi.c9users.io/inregistrare/serbanadi 
Aceasta va sterge din baza de date utilizatorul specificat (/inregistrare/utilizator).
